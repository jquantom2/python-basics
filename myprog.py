# Use the add function from myprog

import os
import json
import pythoadd
import pythoadd as pd
from pythoadd import add

x = pythoadd.add(2, 3)
y = pd.add(2, 45)
z = add(45, 65)


print(f"The value of x is {x}")
print(f"The value of y is {y}")
print(f"The value of z is {z}")

#from pyimport import help_me
#from pyimport import *
from pyimport import help_me, student

help_me()
st = student("quantom", "234", "4th section A")
st.display()

#import pkgs.calculator
#x = pkgs.calculator.calculator()
#y = x.add_int(3,4)

from pkgs.calculator import *
#x = calculator()
#y = x.add_int(3, 4)

#from pkgs.calculator import calculator
#x = calculator()
#y = x.add_int(3, 4)


import pkgs

x = pkgs.calculator.calculator()
y = x.add_int("3", "4")
print(y)

