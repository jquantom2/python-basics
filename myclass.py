# Demonstration of class in python

class Car:
    def __init__(self):
        self.color = "red"
        self.__newcolor = "white"
        self.name = "maruti"
        self.transmission = "manual"
        self.breaktype = "disc"
        self.model = "santro"

    def create(self, color, name, transmission, breaktype):
        self.color = color
        self.name = name
        self.transmission = transmission
        self.breaktype = breaktype

    def get_car_details(self):
        return self.color, self.name, self.transmission, self.breaktype

    def display(self):
        print("Your car details are:")
        print(f"Name         : {self.name}")
        print(f"Color        : {self.color}")
        print(f"breaktype    : {self.breaktype}")
        print(f"transmission : {self.transmission}")
        print(f"new color : {self.__newcolor}")

    @staticmethod
    def displaymethod():
        print("Static Medthod")


mycar = Car()   # mycar is an object (instance of the class car)
mycar.display()
# print(f"The new color of the car is {mycar.__newcolor}") # wrong, will throw error
print(f"The color of the car is {mycar.color}")

mycar.create("blue", "hyundai", "auto", "disc")
mycar.display()

#static method
Car.displaymethod()
