
# Operator overloading
# Overaload the oprators like + - * etc

# Function overloading
# Overload the function

# default arguments
def fun1(val1, val2):
    print(val1)
    print(val2)
fun1(1, 2)

def fun2(val1):
    print(val1)
fun2(1)

def fun(val=16, iscar=True, name="quantom"):
    print(f"val = {val}")
    print(f"iscar = {iscar}")
    print(f"name = {name}")

fun()
fun(12)
fun(12, False)
fun(name='devops')
fun(name='devops training', val=13)
fun(val=67, name="vj", iscar=True)

# Abstract class : Abstract method
# Override the methods.

# Interfaces
class Oracle1:
    def connect (self):
        print('Connecting to Oracle database ...')

    def disconnect(self):
        print('Disconnected from Oracle.')

from abc import *
class Myclass(ABC):

    @abstractmethod
    def connect(self):
        pass

    @abstractmethod
    def disconnect(self):
        pass

class Oracle(Myclass):
    def connect(self):
        print('Connecting to Oracle database ...')
    def disconnect(self ):
        print('Disconnected from Oracle.')

# this is another sub class
class Sybase(Myclass):
    def connect(self):
        print('Connecting to Sybase database ... ')
    def disconnect(self):
        print('Disconnected from Sybase.')


class Database:
    # accept database name as a string
    str = input('Enter database name:')

    # convert the string into classname
    classname = globals()[str]

    # create an object to that class
    x = classname()

    # call the connect () and disconnect () methods
    x.connect()
    x.disconnect()
