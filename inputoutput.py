import sys

# Demonstrating IO in Python
# Functions
def fun():
    print("Hello Function.")

def fun1():
    print("hello fun1.")
    # I am doing operation on add operator
    a = 2
    b = 3
    c = a + b #+ 6
    print(f"{a}+{b} = {c}")
    print("End of fun1.")
    print("hello world.")


def fun2(a, b):
    print("hello fun2.")
    print('Start f2')
    c = a + b
    print(f'{a} + {b} = {c}')

    c = a - b
    print(f'{a} - {b} = {c}')

    print('End f2')

#fun()

# Calling fun1
fun1()

fun2(16, 7)

def dob(num_years):
    print('Start function dob')
    print(f"The dob of person is {2022-num_years}")
    print('End function dob')


#noyears = input("Enter your age in years: ")
#noyears = int(noyears)
#dob(noyears)


print(f"Number of args are {len(sys.argv)}")
print(f"Arguments list : {sys.argv}")
noyears = int(sys.argv[1])
dob(noyears)
