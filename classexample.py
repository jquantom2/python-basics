

# List
# is a sequence/collection datatype
# Datatype : Type of data that it holds
# List is an abstract data type (ADT)

# list vs int data types
a = 5 # some operations of the int data

# uses of list
dl1 = list()
print(f"The list is {dl1}")
dl = [4, 5, 12, 32]

count = dl.count(5)
print(f"The count is {count}")
print(f"THe list is {dl}")
dl.append(56)
print(f"THe new list is {dl}")


