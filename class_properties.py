

# Properties of class
#1. Abstraction
#2. Inheritance
#3. Encapsulation
#4. Polymorphism

# A class have data and methods

#1. Abstraction
# THe inner details are hidden from the user
# example : Car braking. Braking a car has lot of
# functionality that is hidden/abstracted to the user

#2. Inheritance
# Inheriting the properties of parent

# parent class or superclass or base class
class A:
    def __init__(self):
        self.a = 1
        self.b = 2

    def display(self):
        print(" In class A.....")

    def display1(self):
        print(f"Class A: The values of a and b are {self.a} and {self.b}")

# B is subclass or child class
class B(A):

    def __init__(self):
        self.c = 3
        super().__init__()

    def display(self):
        print("In class B")

    def display2(self):
        print(f"Class B : The value of c is {self.c}")

a = A()
a.display1()

b = B()
b.display1()
b.display2()
a.display()
b.display()



# class methods/variables

class A:
    a = 1
    b = 2

    @classmethod
    def methodl(cls):
        print(cls.a)
        print(cls.b)

class B(A):
    c = 3

    @classmethod
    def method2(cls):
        print(cls.c)

    @classmethod
    def method1(cls):
        super().methodl()

#a1 = A()
A.methodl()


B.method2()
print(f"the a val is {B.a}")
B.method1()



# create a class

class car:
    a = 5
    b = 6

    def __init__(self):
        self.x = 14
        self.y = 15

    def display(self):
        print(f"x={self.x}, y={self.y}")

    @classmethod
    def print(cls):
        print(f"a={cls.a}, b={cls.b}")

    @staticmethod
    def disp_any():
        print("disp_any : This is a static method.")

# Calling a object method by an object of the class.
mycar = car()
mycar.display()

# calling a class method by class
car.print()

# calling a static method by class
car.disp_any()

#3. Polymorphism
class Calculator:
    def __init__(self, val):
        self.val = val

    def __add__(self, newval):
        if isinstance(self.val, int) and isinstance(newval, int):
            return self.val + newval
        elif isinstance(self.val, str) and isinstance(newval, str):
            return f"{self.val}{newval}"
        elif isinstance(newval, Calculator):
            if isinstance(self.val, int) and isinstance(newval.val, int):
                return self.val + newval.val
            elif isinstance(self.val, str) and isinstance(newval.val, str):
                return f"{self.val}{newval.val}"
            else:
                raise Exception("Mismatch typees....")
        else:
            raise Exception("Cannot add, mismatch types.")


c1 = Calculator(23)
val = c1 + 67
print(f"The val is {val}")
#val = c1 + "quantom"
#print(f"The val is {val}")

c1 = Calculator("quantom")
val = c1 + "meda"
print(f"The val is {val}")

c1 = Calculator(45)
c2 = Calculator(56)
val = c1 + c2
print(f"The val is {val}")
