
class mylist:
    def __init__(self, *vals):
        self.count = 0
        self.stack = None
        self.__L1 = []
        if vals:
            self.__L1 = list(vals)
            self.count = len(vals)

    def append(self, val):
        # Append at the end of the list
        self.count += 1
        self.__L1.append(val)

    def __str__(self):
        mystr = "["
        for val in self.__L1:
            mystr = f"{mystr},{val}"
        mystr = f"{mystr}]"
        return mystr


ml = mylist()

print(f"The mylist is {ml}")
ml = mylist(3, 6, 12, 43)
print(f"The mylist is {ml}")
print(f"Count = {ml.count}")
ml.append(5987)
print(f"The mylist is {ml}")
print(f"Count = {ml.count}")

